#!/bin/bash
curl -XDELETE localhost:9200/nav
curl -XPUT localhost:9200/nav -H "Content-Type:application/json" --data-binary @nav_mappings.json
curl -XPOST localhost:9200/nav/_bulk?pretty -H "Content-Type:application/json" --data-binary @nav_bulk.json

