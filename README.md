# Backtest_Stochastic

#### Description
CSDN 文章 "在 Elasticsearch 中回测随机(Stochastic)指标交叉策略”的演示材料

#### 软件架构
软件架构说明
Curl shell 脚本和 Python 访问 Elasticsearch 服务器（使用 v7.10.1 测试)

使用说明
执行此脚本将创建nav索引，并将文档数据索引其中。索引nav的数据来自TuShare大数据开放社区，其中选择了54只工银瑞信股票型基金用于演示目的。选择的时间范围在2020-12-01和2021-05-31之间。

./nav_index.sh

Python 安装教程
自动运行 backtest_stochastic.sh 中的 pip install并激活虚拟环境命令

执行此脚本将索引nav下计算基金代码000803.OF在2021-01-01和2021-05-31期间回测 Stochastic 交叉策略。

./backtest_stochastic.sh backtest_stochastic.json 000803.OF F_Type

执行结果生成RSI交叉交易策略统计数据
购买次数:              2
卖出次数:              2
得胜次数:              1
亏损次数:              1
总利润:            -0.13
平均购买价格:  3.24
利润百分率:  -3.89%



参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
