import requests
import json
import sys, getopt
from pprint import pprint

# get data from elasticsearch server 
def get_data(inputfile, symbol):
    url = 'http://localhost:9200/nav/_search?pretty'
    with open(inputfile) as f:
        payload = json.load(f)
    payload_json = json.dumps(payload)
    payload_json = payload_json % symbol
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
    r = requests.post(url, data=payload_json, headers=headers)
    return r.text

# get the command line parameters for the trading policy and the ticker symbol
def get_opt(argv):
    inputfile = 'backtest_stochastic.json'
    symbol = '000803.OF'
    sf_type = 'F_Type'
    try:
        opts, args = getopt.getopt(argv, "hi:s:t:")
    except getopt.GetoptError:
        print('backtest_stochstics -i <inputfile> -s <symbol> -t <fast/slow type>')
        print('example: backtest_stochastics -i backtest_stochastic.json -s 000803.OF -t F_Type/S_Type')
        sys.exit(-1)

    for opt, arg in opts:
        if opt == '-h':
            print('backtest_stochastics -i <inputfile> -s <symbol> -t <fast/slow type>')
            print('example: backtest_rsi -i backtest_stochastic.json -s 000803.OF -t F_Type/S_Type')
            sys.exit(0)
        elif opt in ('-i'):
            inputfile = arg
        elif opt in ('-s'):
            symbol = arg
        elif opt in ('-t'):
            sf_type = arg

    if inputfile == '':
        print("No input file!")
        sys.exit(-1)

    return inputfile, symbol, sf_type


# parse the response data and refine the buy/sell signal
def parse_data(resp, type):
    result = json.loads(resp)
    aggregations = result['aggregations']
    if aggregations and 'Backtest_Stochastic' in aggregations:
        Backtest_Stochastics = aggregations['Backtest_Stochastic']

    transactions = []
    hold = False
    if Backtest_Stochastics and 'buckets' in Backtest_Stochastics:
        for bucket in Backtest_Stochastics['buckets']:
            transaction = {}
            transaction['date'] = bucket['key_as_string']
            transaction['Daily'] = bucket['Daily']['value']
            # honor buy signal if there is no share hold
            if bucket[type]['value'] == -1:
                transaction['original'] = 'buy'
                if not hold:
                    transaction['buy_or_sell'] = 'buy'
                else:
                    transaction['buy_or_sell'] = 'hold'
                hold = True
            # honor sell signal if there is a share hold
            elif bucket[type]['value'] == 1:
                transaction['original'] = 'sell'
                if hold:
                    transaction['buy_or_sell'] = 'sell'
                else:
                    transaction['buy_or_sell'] = 'hold'
                hold = False
            # for other situations, just hold the action
            else:
                transaction['original'] = 'hold'
                transaction['buy_or_sell'] = 'hold'
            transactions.append(transaction)

    return transactions


def report(transactions, type):
    print('Transaction Sequence for : ' + type)
    print('-' * 80)
    pprint(transactions, width=120)
    print('-' * 80)
    print()

    profit = 0.0;
    num_of_buy = 0
    num_of_sell = 0
    buy_price = 0;
    win = 0
    lose = 0
    avg_buy_price = 0
    total_buy_price = 0
    for transaction in transactions:
        if transaction['buy_or_sell'] == 'buy':
           num_of_buy += 1
           buy_price = transaction['Daily']
           profit -= transaction['Daily']
           total_buy_price += buy_price
        elif transaction['buy_or_sell'] == 'sell' and buy_price > 0:
           profit += transaction['Daily']
           if transaction['Daily'] > buy_price:
               win += 1
           else:
               lose += 1
           buy_price = 0
           num_of_sell += 1

    if buy_price > 0:
        profit += transactions[-1]['Daily']
        if transaction['Daily'] > buy_price:
            win += 1
        else:
            lose += 1

    print('购买次数:       %8d' % (num_of_buy))
    print('卖出次数:       %8d' % (num_of_sell))
    print('得胜次数:       %8d' % (win))
    print('亏损次数:       %8d' % (lose))
    print('总利润:         %8.2f' % (profit))
    if total_buy_price > 0:
        avg_buy_price = total_buy_price/num_of_buy
        print('平均购买价格:   %8.2f' % avg_buy_price)
        print('利润百分率:    %8.2f%%' % (profit*100/avg_buy_price))


def main(argv):
    inputfile, symbol, type = get_opt(argv) 
    resp = get_data(inputfile, symbol)
    transactions = parse_data(resp, type)
    report(transactions, type)


if __name__ == '__main__':
    main(sys.argv[1:])
