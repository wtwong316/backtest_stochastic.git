#!/bin/bash
source venv/bin/activate
pip install -r requirements.txt
python backtest_stochastic.py -i $1 -s $2 -t $3

